package net.sf.freecol.server.ai;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(

{ NativeAIPlayerTest.class, CargoTest.class, CacheEntryComparatorTest.class, ColonyPlanTest.class })
public class TestSuite { // nothing
}
