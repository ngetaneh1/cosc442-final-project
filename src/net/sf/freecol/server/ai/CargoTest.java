package net.sf.freecol.server.ai;

import java.util.List;

import javax.annotation.processing.Generated;

import org.junit.Test;
import org.junit.tools.configuration.base.MethodRef;

import net.sf.freecol.common.io.FreeColXMLReader;
import net.sf.freecol.common.io.FreeColXMLWriter;
import net.sf.freecol.common.model.Direction;
import net.sf.freecol.common.model.Game;
import net.sf.freecol.common.model.GoodsType;
import net.sf.freecol.common.model.Specification;
import net.sf.freecol.common.model.Unit;
import net.sf.freecol.server.FreeColServer;
import net.sf.freecol.server.ai.Cargo.CargoMode;

@Generated(value = "org.junit-tools-1.1.0")
public class CargoTest {

	public Cargo createTestSubject() {
		return new Cargo(
				new TransportableAIObject(new AIMain(new FreeColServer(false, false, new Specification(), 0, "")), ""),
				new Unit(new Game(new Specification())), null);
	}

	@MethodRef(name = "initialize", signature = "(QLocation;Z)QString;")
	@Test
	public void testInitialize() throws Exception {
		Cargo testSubject;
		Location destination = null;
		boolean allowFallback = false;
		String result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "initialize", new Object[] { Location.class, allowFallback });
	}

	@MethodRef(name = "update", signature = "()QString;")
	@Test
	public void testUpdate() throws Exception {
		Cargo testSubject;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.update();
	}

	@MethodRef(name = "newCargo", signature = "(QTransportableAIObject;QUnit;)QCargo;")
	@Test
	public void testNewCargo() throws Exception {
		TransportableAIObject t = null;
		Unit carrier = null;
		Cargo result;

		// default test
		result = Cargo.newCargo(t, carrier);
	}

	@MethodRef(name = "newCargo", signature = "(QTransportableAIObject;QUnit;QLocation;Z)QCargo;")
	@Test
	public void testNewCargo_1() throws Exception {
		TransportableAIObject t = null;
		Unit carrier = null;
		Location destination = null;
		boolean allowFallback = false;
		Cargo result;

		// default test
		result = Cargo.newCargo(t, carrier, destination, allowFallback);
	}

	@MethodRef(name = "dump", signature = "()QString;")
	@Test
	public void testDump() throws Exception {
		Cargo testSubject;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.dump();
	}

	@MethodRef(name = "getTransportable", signature = "()QTransportableAIObject;")
	@Test
	public void testGetTransportable() throws Exception {
		Cargo testSubject;
		TransportableAIObject result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getTransportable();
	}

	@MethodRef(name = "getCarrier", signature = "()QUnit;")
	@Test
	public void testGetCarrier() throws Exception {
		Cargo testSubject;
		Unit result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getCarrier();
	}

	@MethodRef(name = "getTries", signature = "()I")
	@Test
	public void testGetTries() throws Exception {
		Cargo testSubject;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getTries();
	}

	@MethodRef(name = "getSpaceLeft", signature = "()I")
	@Test
	public void testGetSpaceLeft() throws Exception {
		Cargo testSubject;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getSpaceLeft();
	}

	@MethodRef(name = "setSpaceLeft", signature = "(I)V")
	@Test
	public void testSetSpaceLeft() throws Exception {
		Cargo testSubject;
		int spaceLeft = 0;

		// default test
		testSubject = createTestSubject();
		testSubject.setSpaceLeft(spaceLeft);
	}

	@MethodRef(name = "isValid", signature = "()Z")
	@Test
	public void testIsValid() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isValid();
	}

	@MethodRef(name = "getMode", signature = "()QCargoMode;")
	@Test
	public void testGetMode() throws Exception {
		Cargo testSubject;
		CargoMode result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getMode();
	}

	@MethodRef(name = "getModeString", signature = "()QString;")
	@Test
	public void testGetModeString() throws Exception {
		Cargo testSubject;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getModeString();
	}

	@MethodRef(name = "getTurns", signature = "()I")
	@Test
	public void testGetTurns() throws Exception {
		Cargo testSubject;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getTurns();
	}

	@MethodRef(name = "isFallback", signature = "()Z")
	@Test
	public void testIsFallback() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isFallback();
	}

	@MethodRef(name = "getTransportTarget", signature = "()QLocation;")
	@Test
	public void testGetTransportTarget() throws Exception {
		Cargo testSubject;
		Location result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getTransportTarget();
	}

	@MethodRef(name = "getCarrierTarget", signature = "()QLocation;")
	@Test
	public void testGetCarrierTarget() throws Exception {
		Cargo testSubject;
		Location result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getCarrierTarget();
	}

	@MethodRef(name = "clear", signature = "()V")
	@Test
	public void testClear() throws Exception {
		Cargo testSubject;

		// default test
		testSubject = createTestSubject();
		testSubject.clear();
	}

	@MethodRef(name = "isCarried", signature = "()Z")
	@Test
	public void testIsCarried() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isCarried();
	}

	@MethodRef(name = "isCollectable", signature = "()Z")
	@Test
	public void testIsCollectable() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isCollectable();
	}

	@MethodRef(name = "isDeliverable", signature = "()Z")
	@Test
	public void testIsDeliverable() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isDeliverable();
	}

	@MethodRef(name = "isDelivered", signature = "()Z")
	@Test
	public void testIsDelivered() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isDelivered();
	}

	@MethodRef(name = "hasPath", signature = "()Z")
	@Test
	public void testHasPath() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.hasPath();
	}

	@MethodRef(name = "getJoinDirection", signature = "()QDirection;")
	@Test
	public void testGetJoinDirection() throws Exception {
		Cargo testSubject;
		Direction result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getJoinDirection();
	}

	@MethodRef(name = "getLeaveDirection", signature = "()QDirection;")
	@Test
	public void testGetLeaveDirection() throws Exception {
		Cargo testSubject;
		Direction result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getLeaveDirection();
	}

	@MethodRef(name = "getNewSpace", signature = "()I")
	@Test
	public void testGetNewSpace() throws Exception {
		Cargo testSubject;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getNewSpace();
	}

	@MethodRef(name = "hasWrapped", signature = "()Z")
	@Test
	public void testHasWrapped() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.hasWrapped();
	}

	@MethodRef(name = "couldWrap", signature = "(QCargo;)Z")
	@Test
	public void testCouldWrap() throws Exception {
		Cargo testSubject;
		Cargo other = null;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.couldWrap(other);
	}

	@MethodRef(name = "wrap", signature = "(QCargo;)V")
	@Test
	public void testWrap() throws Exception {
		Cargo testSubject;
		Cargo other = null;

		// default test
		testSubject = createTestSubject();
		testSubject.wrap(other);
	}

	@MethodRef(name = "unwrap", signature = "()QList<QCargo;>;")
	@Test
	public void testUnwrap() throws Exception {
		Cargo testSubject;
		List<Cargo> result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.unwrap();
	}

	@MethodRef(name = "retry", signature = "()Z")
	@Test
	public void testRetry() throws Exception {
		Cargo testSubject;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.retry();
	}

	@MethodRef(name = "resetTries", signature = "()V")
	@Test
	public void testResetTries() throws Exception {
		Cargo testSubject;

		// default test
		testSubject = createTestSubject();
		testSubject.resetTries();
	}

	@MethodRef(name = "isEuropeanTrade", signature = "(QGoodsType;)Z")
	@Test
	public void testIsEuropeanTrade() throws Exception {
		Cargo testSubject;
		GoodsType type = null;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.isEuropeanTrade(type);
	}

	@MethodRef(name = "check", signature = "(QAIUnit;)QString;")
	@Test
	public void testCheck() throws Exception {
		Cargo testSubject;
		AIUnit aiCarrier = null;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.check(aiCarrier);
	}

	@MethodRef(name = "canQueueAt", signature = "(QUnit;IQList<QCargo;>;)Z")
	@Test
	public void testCanQueueAt() throws Exception {
		Cargo testSubject;
		Unit carrier = null;
		int index = 0;
		List<Cargo> cargoes = null;
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.canQueueAt(carrier, index, cargoes);
	}

	@MethodRef(name = "toShortString", signature = "()QString;")
	@Test
	public void testToShortString() throws Exception {
		Cargo testSubject;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.toShortString();
	}

	@MethodRef(name = "toString", signature = "()QString;")
	@Test
	public void testToString() throws Exception {
		Cargo testSubject;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.toString();
	}

	@MethodRef(name = "toXML", signature = "(QFreeColXMLWriter;)V")
	@Test
	public void testToXML() throws Exception {
		Cargo testSubject;
		FreeColXMLWriter xw = null;

		// default test
		testSubject = createTestSubject();
		testSubject.toXML(xw);
	}

	@MethodRef(name = "readFromXML", signature = "(QAIMain;QFreeColXMLReader;)V")
	@Test
	public void testReadFromXML() throws Exception {
		Cargo testSubject;
		AIMain aiMain = null;
		FreeColXMLReader xr = null;

		// default test
		testSubject = createTestSubject();
		testSubject.readFromXML(aiMain, xr);
	}

	@MethodRef(name = "getXMLElementTagName", signature = "()QString;")
	@Test
	public void testGetXMLElementTagName() throws Exception {
		String result;

		// default test
		result = Cargo.getXMLElementTagName();
	}
}