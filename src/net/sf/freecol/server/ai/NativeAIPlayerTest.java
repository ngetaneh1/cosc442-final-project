package net.sf.freecol.server.ai;

import static org.junit.Assert.*;
import java.util.*;
import org.junit.Assert;
import org.junit.Test;

@Generated(value = "org.junit-tools-1.1.0")
public class NativeAIPlayerTest {

	private NativeAIPlayer createTestSubject() {
		return new NativeAIPlayer(new AIMain(new FreeColServer(false, false, new Specification(), 0, "")),
				new ServerPlayer(new Game(new Specification()), ""));
	}

	@MethodRef(name = "initializeMissions", signature = "(QLogBuilder;)V")
	@Test
	public void testInitializeMissions() throws Exception {
		NativeAIPlayer testSubject;
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "initializeMissions", new Object[] { LogBuilder.class });
	}

	@MethodRef(name="defMissionAssign", signature="(QLogBuilder;QPlayer;QList<QUnit;>;)V")
	@Test
	public void testDefMissionAssign() throws Exception {
	NativeAIPlayer testSubject;LogBuilder lb = null;
	Player player = null;
	List<Unit> units = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"defMissionAssign", new Object[]{LogBuilder.class, Player.class, List<Unit>.class});
	}

	@MethodRef(name="assignDefMission", signature="(QLogBuilder;QList<QUnit;>;QIndianSettlement;)V")
	@Test
	public void testAssignDefMission() throws Exception {
	NativeAIPlayer testSubject;LogBuilder lb = null;
	List<Unit> units = null;
	IndianSettlement is = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"assignDefMission", new Object[]{LogBuilder.class, List<Unit>.class, IndianSettlement.class});
	}

	@MethodRef(name="reqDefCheck", signature="(QLogBuilder;QList<QUnit;>;QIndianSettlement;)V")
	@Test
	public void testReqDefCheck() throws Exception {
	NativeAIPlayer testSubject;LogBuilder lb = null;
	List<Unit> units = null;
	IndianSettlement is = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"reqDefCheck", new Object[]{LogBuilder.class, List<Unit>.class, IndianSettlement.class});
	}

	@MethodRef(name = "determineStances", signature = "(QLogBuilder;)V")
	@Test
	public void testDetermineStances() throws Exception {
		NativeAIPlayer testSubject;
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "determineStances", new Object[] { LogBuilder.class });
	}

	@MethodRef(name = "determineStanceCheck", signature = "(QLogBuilder;QServerPlayer;)V")
	@Test
	public void testDetermineStanceCheck() throws Exception {
		NativeAIPlayer testSubject;
		LogBuilder lb = null;
		ServerPlayer serverPlayer = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "determineStanceCheck",
				new Object[] { LogBuilder.class, ServerPlayer.class });
	}

	@MethodRef(name = "secureSettlements", signature = "([IQLogBuilder;)V")
	@Test
	public void testSecureSettlements() throws Exception {
		NativeAIPlayer testSubject;
		int[] randoms = new int[] { 0 };
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "secureSettlements", new Object[] { randoms, LogBuilder.class });
	}

	@MethodRef(name="changeSettlementSize", signature="(QLogBuilder;QList<QIndianSettlement;>;)V")
	@Test
	public void testChangeSettlementSize() throws Exception {
	NativeAIPlayer testSubject;LogBuilder lb = null;
	List<IndianSettlement> settlements = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"changeSettlementSize", new Object[]{LogBuilder.class, List<IndianSettlement>.class});
	}

	@MethodRef(name="spreadArmsAndHorses", signature="([IIQList<QIndianSettlement;>;)V")
	@Test
	public void testSpreadArmsAndHorses() throws Exception {
	NativeAIPlayer testSubject;int[] randoms = new int[]{0};
	int randomIdx = 0;
	List<IndianSettlement> settlements = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"spreadArmsAndHorses", new Object[]{randoms, randomIdx, List<IndianSettlement>.class});
	}

	@MethodRef(name = "equipBraves", signature = "(QIndianSettlement;QLogBuilder;)V")
	@Test
	public void testEquipBraves() throws Exception {
		NativeAIPlayer testSubject;
		IndianSettlement is = null;
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		testSubject.equipBraves(is, lb);
	}

	@MethodRef(name="militaryImproveCheck", signature="(QIndianSettlement;QLogBuilder;QList<QUnit;>;)V")
	@Test
	public void testMilitaryImproveCheck() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	LogBuilder lb = null;
	List<Unit> units = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"militaryImproveCheck", new Object[]{IndianSettlement.class, LogBuilder.class, List<Unit>.class});
	}

	@MethodRef(name = "secureIndianSettlement", signature = "(QIndianSettlement;QLogBuilder;)V")
	@Test
	public void testSecureIndianSettlement() throws Exception {
		NativeAIPlayer testSubject;
		IndianSettlement is = null;
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		testSubject.secureIndianSettlement(is, lb);
	}

	@MethodRef(name="assignAttackUnits", signature="(QLogBuilder;QAIMain;QList<QUnit;>;QList<QTile;>;)V")
	@Test
	public void testAssignAttackUnits() throws Exception {
	NativeAIPlayer testSubject;LogBuilder lb = null;
	AIMain aiMain = null;
	List<Unit> units = null;
	List<Tile> threatTiles = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"assignAttackUnits", new Object[]{LogBuilder.class, AIMain.class, List<Unit>.class, List<Tile>.class});
	}

	@MethodRef(name="checkForClosestUnit", signature="(QAIMain;QList<QUnit;>;QTile;IQUnit;)QUnit;")
	@Test
	public void testCheckForClosestUnit() throws Exception {
	NativeAIPlayer testSubject;AIMain aiMain = null;
	List<Unit> units = null;
	Tile tile = null;
	int bestDistance = 0;
	Unit unit = null;
	Unit result;
	
	// default test
	testSubject=createTestSubject();result=Whitebox.invokeMethod(testSubject,"checkForClosestUnit", new Object[]{AIMain.class, List<Unit>.class, Tile.class, bestDistance, Unit.class});
	}

	@MethodRef(name="checkDefenderNeed", signature="(QIndianSettlement;QLogBuilder;QAIMain;QList<QUnit;>;QList<QUnit;>;QComparator<QUnit;>;I)V")
	@Test
	public void testCheckDefenderNeed() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	LogBuilder lb = null;
	AIMain aiMain = null;
	List<Unit> units = null;
	List<Unit> defenders = null;
	Comparator<Unit> isComparator = null;
	int needed = 0;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"checkDefenderNeed", new Object[]{IndianSettlement.class, LogBuilder.class, AIMain.class, List<Unit>.class, List<Unit>.class, Comparator<Unit>.class, needed});
	}

	@MethodRef(name="collectThreats", signature="(QIndianSettlement;QAIMain;QPlayer;QCombatModel;QList<QUnit;>;QList<QUnit;>;QHashMap<QTile;QDouble;>;)V")
	@Test
	public void testCollectThreats() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	AIMain aiMain = null;
	Player player = null;
	CombatModel cm = null;
	List<Unit> units = null;
	List<Unit> defenders = null;
	HashMap<Tile,Double> threats = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"collectThreats", new Object[]{IndianSettlement.class, AIMain.class, Player.class, CombatModel.class, List<Unit>.class, List<Unit>.class, HashMap<Tile,Double>.class});
	}

	@MethodRef(name="threatEvaluationLoop", signature="(QIndianSettlement;QAIMain;QPlayer;QCombatModel;QList<QUnit;>;QList<QUnit;>;QHashMap<QTile;QDouble;>;)V")
	@Test
	public void testThreatEvaluationLoop() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	AIMain aiMain = null;
	Player player = null;
	CombatModel cm = null;
	List<Unit> units = null;
	List<Unit> defenders = null;
	HashMap<Tile,Double> threats = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"threatEvaluationLoop", new Object[]{IndianSettlement.class, AIMain.class, Player.class, CombatModel.class, List<Unit>.class, List<Unit>.class, HashMap<Tile,Double>.class});
	}

	@MethodRef(name="endingThreatEval", signature="(QIndianSettlement;QCombatModel;QHashMap<QTile;QDouble;>;QTile;DDD)V")
	@Test
	public void testEndingThreatEval() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	CombatModel cm = null;
	HashMap<Tile,Double> threats = null;
	Tile t = null;
	double threshold = 0.0;
	double bonus = 0.0;
	double value = 0.0;
	
	
	// test 1
	testSubject=createTestSubject();value = 0.0;
	Whitebox.invokeMethod(testSubject,"endingThreatEval", new Object[]{IndianSettlement.class, CombatModel.class, HashMap<Tile,Double>.class, Tile.class, threshold, bonus, value});
	
	// test 2
	testSubject=createTestSubject();value = -1.0;
	Whitebox.invokeMethod(testSubject,"endingThreatEval", new Object[]{IndianSettlement.class, CombatModel.class, HashMap<Tile,Double>.class, Tile.class, threshold, bonus, value});
	
	// test 3
	testSubject=createTestSubject();value = 1.0;
	Whitebox.invokeMethod(testSubject,"endingThreatEval", new Object[]{IndianSettlement.class, CombatModel.class, HashMap<Tile,Double>.class, Tile.class, threshold, bonus, value});
	}

	@MethodRef(name="defenderContainsCheck", signature="(QIndianSettlement;QAIMain;QList<QUnit;>;QList<QUnit;>;QTile;)V")
	@Test
	public void testDefenderContainsCheck() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	AIMain aiMain = null;
	List<Unit> units = null;
	List<Unit> defenders = null;
	Tile t = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"defenderContainsCheck", new Object[]{IndianSettlement.class, AIMain.class, List<Unit>.class, List<Unit>.class, Tile.class});
	}

	@MethodRef(name="collectDefenders", signature="(QIndianSettlement;QAIMain;QList<QUnit;>;QList<QUnit;>;)V")
	@Test
	public void testCollectDefenders() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	AIMain aiMain = null;
	List<Unit> units = null;
	List<Unit> defenders = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"collectDefenders", new Object[]{IndianSettlement.class, AIMain.class, List<Unit>.class, List<Unit>.class});
	}

	@MethodRef(name="ownedUnitsCheck", signature="(QIndianSettlement;QList<QUnit;>;)V")
	@Test
	public void testOwnedUnitsCheck() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	List<Unit> units = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"ownedUnitsCheck", new Object[]{IndianSettlement.class, List<Unit>.class});
	}

	@MethodRef(name = "giveNormalMissions", signature = "(QLogBuilder;)V")
	@Test
	public void testGiveNormalMissions() throws Exception {
		NativeAIPlayer testSubject;
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "giveNormalMissions", new Object[] { LogBuilder.class });
	}

	@MethodRef(name="unitSettlementCompare", signature="(QLogBuilder;QList<QAIUnit;>;QList<QAIUnit;>;)V")
	@Test
	public void testUnitSettlementCompare() throws Exception {
	NativeAIPlayer testSubject;LogBuilder lb = null;
	List<AIUnit> aiUnits = null;
	List<AIUnit> done = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"unitSettlementCompare", new Object[]{LogBuilder.class, List<AIUnit>.class, List<AIUnit>.class});
	}

	@MethodRef(name="checkUnitMissionNeed", signature="(QList<QAIUnit;>;QList<QAIUnit;>;)V")
	@Test
	public void testCheckUnitMissionNeed() throws Exception {
	NativeAIPlayer testSubject;List<AIUnit> aiUnits = null;
	List<AIUnit> done = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"checkUnitMissionNeed", new Object[]{List<AIUnit>.class, List<AIUnit>.class});
	}

	@MethodRef(name = "bringGifts", signature = "([IQLogBuilder;)V")
	@Test
	public void testBringGifts() throws Exception {
		NativeAIPlayer testSubject;
		int[] randoms = new int[] { 0 };
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "bringGifts", new Object[] { randoms, LogBuilder.class });
	}

	@MethodRef(name = "mainGiftChecker", signature = "([IQLogBuilder;QPlayer;QCostDecider;II)V")
	@Test
	public void testMainGiftChecker() throws Exception {
		NativeAIPlayer testSubject;
		int[] randoms = new int[] { 0 };
		LogBuilder lb = null;
		Player player = null;
		CostDecider cd = null;
		int giftProbability = 0;
		int randomIdx = 0;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "mainGiftChecker", new Object[] { randoms, LogBuilder.class, Player.class,
				CostDecider.class, giftProbability, randomIdx });
	}

	@MethodRef(name = "invalidReasonCheck", signature = "(QCostDecider;QUnit;QAIUnit;QTile;QUnit;)QUnit;")
	@Test
	public void testInvalidReasonCheck() throws Exception {
		NativeAIPlayer testSubject;
		CostDecider cd = null;
		Unit unit = null;
		AIUnit aiUnit = null;
		Tile home = null;
		Unit u = null;
		Unit result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "invalidReasonCheck",
				new Object[] { CostDecider.class, Unit.class, AIUnit.class, Tile.class, Unit.class });
	}

	@MethodRef(name = "nullTargetThrow", signature = "(QColony;)V")
	@Test
	public void testNullTargetThrow() throws Exception {
		NativeAIPlayer testSubject;
		Colony target = null;

		// test 1
		testSubject = createTestSubject();
		target = null;
		Whitebox.invokeMethod(testSubject, "nullTargetThrow", new Object[] { Colony.class });
	}

	@MethodRef(name="collectColonies", signature="(QCostDecider;QIndianSettlement;QUnit;QAIUnit;QTile;QList<QRandomChoice<QColony;>;>;)V")
	@Test
	public void testCollectColonies() throws Exception {
	NativeAIPlayer testSubject;CostDecider cd = null;
	IndianSettlement is = null;
	Unit unit = null;
	AIUnit aiUnit = null;
	Tile home = null;
	List<RandomChoice<Colony>> nearbyColonies = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"collectColonies", new Object[]{CostDecider.class, IndianSettlement.class, Unit.class, AIUnit.class, Tile.class, List<RandomChoice<Colony>>.class});
	}

	@MethodRef(name="ownedUnitsforGiftCheck", signature="(QIndianSettlement;QList<QUnit;>;I)I")
	@Test
	public void testOwnedUnitsforGiftCheck() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	List<Unit> availableUnits = null;
	int alreadyAssignedUnits = 0;
	int result;
	
	// default test
	testSubject=createTestSubject();result=Whitebox.invokeMethod(testSubject,"ownedUnitsforGiftCheck", new Object[]{IndianSettlement.class, List<Unit>.class, alreadyAssignedUnits});
	}

	@MethodRef(name = "demandTribute", signature = "([IQLogBuilder;)V")
	@Test
	public void testDemandTribute() throws Exception {
		NativeAIPlayer testSubject;
		int[] randoms = new int[] { 0 };
		LogBuilder lb = null;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "demandTribute", new Object[] { randoms, LogBuilder.class });
	}

	@MethodRef(name = "demandGiftChecker", signature = "([IQLogBuilder;QPlayer;QCostDecider;II)V")
	@Test
	public void testDemandGiftChecker() throws Exception {
		NativeAIPlayer testSubject;
		int[] randoms = new int[] { 0 };
		LogBuilder lb = null;
		Player player = null;
		CostDecider cd = null;
		int demandProbability = 0;
		int randomIdx = 0;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "demandGiftChecker", new Object[] { randoms, LogBuilder.class, Player.class,
				CostDecider.class, demandProbability, randomIdx });
	}

	@MethodRef(name="alreadyAssnUnitsCheck", signature="(QIndianSettlement;QList<QUnit;>;)I")
	@Test
	public void testAlreadyAssnUnitsCheck() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	List<Unit> availableUnits = null;
	int result;
	
	// default test
	testSubject=createTestSubject();result=Whitebox.invokeMethod(testSubject,"alreadyAssnUnitsCheck", new Object[]{IndianSettlement.class, List<Unit>.class});
	}

	@MethodRef(name="demandGiftColonyChecker", signature="(QCostDecider;QIndianSettlement;QTile;QUnit;QAIUnit;QList<QRandomChoice<QColony;>;>;)V")
	@Test
	public void testDemandGiftColonyChecker() throws Exception {
	NativeAIPlayer testSubject;CostDecider cd = null;
	IndianSettlement is = null;
	Tile home = null;
	Unit unit = null;
	AIUnit aiUnit = null;
	List<RandomChoice<Colony>> nearbyColonies = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"demandGiftColonyChecker", new Object[]{CostDecider.class, IndianSettlement.class, Tile.class, Unit.class, AIUnit.class, List<RandomChoice<Colony>>.class});
	}

	@MethodRef(name="demandGiftUnitChecker", signature="(QIndianSettlement;QList<QUnit;>;I)I")
	@Test
	public void testDemandGiftUnitChecker() throws Exception {
	NativeAIPlayer testSubject;IndianSettlement is = null;
	List<Unit> availableUnits = null;
	int alreadyAssignedUnits = 0;
	int result;
	
	// default test
	testSubject=createTestSubject();result=Whitebox.invokeMethod(testSubject,"demandGiftUnitChecker", new Object[]{IndianSettlement.class, List<Unit>.class, alreadyAssignedUnits});
	}

	@MethodRef(name = "getShipTradePenalties", signature = "(Z)QSet<QModifier;>;")
	@Test
	public void testGetShipTradePenalties() throws Exception {
		NativeAIPlayer testSubject;
		boolean sense = false;
		Set<Modifier> result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "getShipTradePenalties", new Object[] { sense });
	}

	@MethodRef(name="shipTradePenaltyLoop", signature="(ZQSpecification;IQSet<QModifier;>;)V")
	@Test
	public void testShipTradePenaltyLoop() throws Exception {
	NativeAIPlayer testSubject;boolean sense = false;
	Specification spec = null;
	int penalty = 0;
	Set<Modifier> result = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"shipTradePenaltyLoop", new Object[]{sense, Specification.class, penalty, Set<Modifier>.class});
	}

	@MethodRef(name = "abortInvalidMissions", signature = "()V")
	@Test
	public void testAbortInvalidMissions() throws Exception {
		NativeAIPlayer testSubject;

		// default test
		testSubject = createTestSubject();
		testSubject.abortInvalidMissions();
	}

	@MethodRef(name = "startWorking", signature = "()V")
	@Test
	public void testStartWorking() throws Exception {
		NativeAIPlayer testSubject;

		// default test
		testSubject = createTestSubject();
		testSubject.startWorking();
	}

	@MethodRef(name = "workAssigner", signature = "(IQRandom;QLogBuilder;)QList<QAIUnit;>;")
	@Test
	public void testWorkAssigner() throws Exception {
		NativeAIPlayer testSubject;
		int nSettlements = 0;
		Random air = null;
		LogBuilder lb = null;
		List<AIUnit> result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "workAssigner",
				new Object[] { nSettlements, Random.class, LogBuilder.class });
	}

	@MethodRef(name = "adjustMission", signature = "(QAIUnit;QPathNode;QClass;I)I")
	@Test
	public void testAdjustMission() throws Exception {
		NativeAIPlayer testSubject;
		AIUnit aiUnit = null;
		PathNode path = null;
		Class type = null;
		int value = 0;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.adjustMission(aiUnit, path, type, value);
	}

	@MethodRef(name = "registerSellGoods", signature = "(QGoods;)V")
	@Test
	public void testRegisterSellGoods() throws Exception {
		NativeAIPlayer testSubject;
		Goods goods = null;

		// default test
		testSubject = createTestSubject();
		testSubject.registerSellGoods(goods);
	}

	@MethodRef(name = "buyProposition", signature = "(QUnit;QSettlement;QGoods;I)I")
	@Test
	public void testBuyProposition() throws Exception {
		NativeAIPlayer testSubject;
		Unit unit = null;
		Settlement settlement = null;
		Goods goods = null;
		int gold = 0;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.buyProposition(unit, settlement, goods, gold);
	}

	@MethodRef(name = "hagglingCheck", signature = "(QString;)I")
	@Test
	public void testHagglingCheck() throws Exception {
		NativeAIPlayer testSubject;
		String hagglingKey = "";
		int result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "hagglingCheck", new Object[] { hagglingKey });
	}

	@MethodRef(name = "goldHagglingAddition", signature = "(IQString;QString;I)V")
	@Test
	public void testGoldHagglingAddition() throws Exception {
		NativeAIPlayer testSubject;
		int gold = 0;
		String goldKey = "";
		String hagglingKey = "";
		int haggling = 0;

		// default test
		testSubject = createTestSubject();
		Whitebox.invokeMethod(testSubject, "goldHagglingAddition",
				new Object[] { gold, goldKey, hagglingKey, haggling });
	}

	@MethodRef(name = "keyCheck", signature = "(QString;I)I")
	@Test
	public void testKeyCheck() throws Exception {
		NativeAIPlayer testSubject;
		String hagglingKey = "";
		int haggling = 0;
		int result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "keyCheck", new Object[] { hagglingKey, haggling });
	}

	@MethodRef(name = "modifierSets", signature = "(QUnit;QSpecification;QIndianSettlement;QPlayer;QString;I)I")
	@Test
	public void testModifierSets() throws Exception {
		NativeAIPlayer testSubject;
		Unit unit = null;
		Specification spec = null;
		IndianSettlement is = null;
		Player buyer = null;
		String goldKey = "";
		int price = 0;
		int result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "modifierSets",
				new Object[] { Unit.class, Specification.class, IndianSettlement.class, Player.class, goldKey, price });
	}

	@MethodRef(name="navalConditional", signature="(QUnit;QSet<QModifier;>;)V")
	@Test
	public void testNavalConditional() throws Exception {
	NativeAIPlayer testSubject;Unit unit = null;
	Set<Modifier> modifiers = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"navalConditional", new Object[]{Unit.class, Set<Modifier>.class});
	}

	@MethodRef(name="missionaryConditional", signature="(QSpecification;QIndianSettlement;QPlayer;QSet<QModifier;>;)V")
	@Test
	public void testMissionaryConditional() throws Exception {
	NativeAIPlayer testSubject;Specification spec = null;
	IndianSettlement is = null;
	Player buyer = null;
	Set<Modifier> modifiers = null;
	
	
	// default test
	testSubject=createTestSubject();Whitebox.invokeMethod(testSubject,"missionaryConditional", new Object[]{Specification.class, IndianSettlement.class, Player.class, Set<Modifier>.class});
	}

	@MethodRef(name = "sellProposition", signature = "(QUnit;QSettlement;QGoods;I)I")
	@Test
	public void testSellProposition() throws Exception {
		NativeAIPlayer testSubject;
		Unit unit = null;
		Settlement settlement = null;
		Goods goods = null;
		int gold = 0;
		int result;

		// test 1
		testSubject = createTestSubject();
		gold = 0;
		result = testSubject.sellProposition(unit, settlement, goods, gold);
		Assert.assertEquals(0, result, 0);

		// test 2
		testSubject = createTestSubject();
		gold = -1;
		result = testSubject.sellProposition(unit, settlement, goods, gold);
		Assert.assertEquals(0, result, 0);

		// test 3
		testSubject = createTestSubject();
		gold = 1;
		result = testSubject.sellProposition(unit, settlement, goods, gold);
		Assert.assertEquals(0, result, 0);
	}

	@MethodRef(name = "propositionModifierEval", signature = "(QUnit;QSpecification;QIndianSettlement;QPlayer;I)I")
	@Test
	public void testPropositionModifierEval() throws Exception {
		NativeAIPlayer testSubject;
		Unit unit = null;
		Specification spec = null;
		IndianSettlement is = null;
		Player seller = null;
		int price = 0;
		int result;

		// default test
		testSubject = createTestSubject();
		result = Whitebox.invokeMethod(testSubject, "propositionModifierEval",
				new Object[] { Unit.class, Specification.class, IndianSettlement.class, Player.class, price });
	}

	@MethodRef(name = "getXMLTagName", signature = "()QString;")
	@Test
	public void testGetXMLTagName() throws Exception {
		NativeAIPlayer testSubject;
		String result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getXMLTagName();
	}
}