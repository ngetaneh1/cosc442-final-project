package net.sf.freecol.server.ai;

import javax.annotation.processing.Generated;

import org.junit.Test;
import org.junit.tools.configuration.base.MethodRef;

@Generated(value = "org.junit-tools-1.1.0")
public class CacheEntryComparatorTest {

	private CacheEntryComparator createTestSubject() {
		return new CacheEntryComparator();
	}

	@MethodRef(name = "compareProduction", signature = "(QProductionCache.Entry;QProductionCache.Entry;)I")
	@Test
	public void testCompareProduction() throws Exception {
		CacheEntryComparator testSubject;
		Entry entry1 = null;
		Entry entry2 = null;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.compareProduction(entry1, entry2);
	}

	@MethodRef(name = "compare", signature = "(QProductionCache.Entry;QProductionCache.Entry;)I")
	@Test
	public void testCompare() throws Exception {
		CacheEntryComparator testSubject;
		Entry entry1 = null;
		Entry entry2 = null;
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.compare(entry1, entry2);
	}
}