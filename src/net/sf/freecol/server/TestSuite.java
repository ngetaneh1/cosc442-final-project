package net.sf.freecol.server;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(

{ net.sf.freecol.server.ai.TestSuite.class })
public class TestSuite { // nothing
}
