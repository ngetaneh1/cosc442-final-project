package net.sf.freecol.client.gui;

        import net.sf.freecol.client.FreeColClient;
        import net.sf.freecol.client.gui.Canvas;
        import net.sf.freecol.client.gui.GUI;

        import java.awt.event.MouseWheelEvent;
        import java.awt.event.MouseWheelListener;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving canvasMouseWheel events.
 * The class that is interested in processing a canvasMouseWheel
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addCanvasMouseWheelListener<code> method. When
 * the canvasMouseWheel event occurs, that object's appropriate
 * method is invoked.
 *
 * @see CanvasMouseWheelEvent
 */
public final class CanvasMouseWheelListener implements MouseWheelListener {


    /** The free col client. */
    private final FreeColClient freeColClient;

    /** The canvas. */
    private final Canvas canvas;


    /**
     * Create a new canvas mouse listener.
     *
     * @param freeColClient The enclosing <code>FreeColClient</code>.
     * @param canvas The component this object gets created for.
     */
    public CanvasMouseWheelListener(FreeColClient freeColClient, Canvas canvas) {
        this.freeColClient = freeColClient;
        this.canvas = canvas;
    }

    /**
     * Get the GUI.
     *
     * @return The GUI.
     */
    private GUI getGUI() {
        return freeColClient.getGUI();
    }
    
    
    /**
     * Mouse wheel moved.
     *
     * @param e the e
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
    	//zoom in when mouse wheel goes up, and zoom out when mouse wheel goes down.
        int notches = e.getWheelRotation();
        if (notches < 0) {
            getGUI().zoomInMap();

        } else {
            getGUI().zoomOutMap();
        }
    }
}
